# download and save stanford corenlp chinese model file as /tmp/chinese-corenlp-models.jar
# SparkContext.addJar("/tmp/chinese-corenlp-models.jar")

// https://databricks-prod-cloudfront.cloud.databricks.com/public/4027ec902e239c93eaaa8714f173bcfc/1233855/3233914658600709/588180/latest.html
// http://www.robertomarchetto.com/spark_java_maven_example

spark-submit --class org.sparkexample.WordCountTask build/libs/first-example-1.0-SNAPSHOT.jar infile.txt

